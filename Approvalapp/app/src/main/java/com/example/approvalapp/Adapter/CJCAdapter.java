package com.example.approvalapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.approvalapp.Model.CJCParam;
import com.example.approvalapp.R;
/*import com.example.approvalapp.activity.CJC_FormActivity;*/

import java.util.ArrayList;

public class CJCAdapter extends RecyclerView.Adapter<CJCAdapter.MyViewHolder> {
    Context context;
    ArrayList<CJCParam> CJCArrayList;

    public CJCAdapter(Context context, ArrayList<CJCParam> functionsArrayList)
    {
        this.context = context;
        this.CJCArrayList =functionsArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.cjc_layout,viewGroup,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
//        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position)
    {

        final CJCParam function = CJCArrayList.get(holder.getAdapterPosition());
        holder.txtCJCNo.setText(function.getCJCNo());
        holder.txtOrderNo.setText(function.getOrderNo());
        holder.txtCJCDate.setText(function.getCJCDate());
        holder.txtOrderValue.setText(function.getOrderValue());
        holder.txtCJCAmount.setText(function.getAmount());
        holder.txtBussinessPartenName.setText(function.getBussineePartnerName());
        holder.txtInitaiatedBy.setText(function.getCheckedByName());
        holder.txtcertifier.setText(function.getCertifierName());
        holder.txtApprover.setText(function.getApproverName());
        holder.txtDateOfCompletion.setText(function.getDateOfCompletion());
        holder.txtPaymentDueDate.setText(function.getPaymentDueDate());

        /*if (!function.getColorCode().startsWith("#"))
        {
            holder.cv_function_card.setCardBackgroundColor(Color.parseColor("#".concat(function.getColorCode().trim())));
        }
        else
        {
            holder.cv_function_card.setCardBackgroundColor(Color.parseColor(function.getColorCode()));
        }*/
//        holder.functionId = function.getFunctionId();

        /*holder.img_rightArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(context,ReportView.class);
                Bundle param=new Bundle();
                param.putString("functionId",function.getFunctionId());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });*/

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("CJCNo",function.getCJCNo());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });
*/        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("OrderNo",function.getOrderNo());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("CJCDate",function.getCJCDate());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("OrderValue",function.getOrderValue());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("CJCAmount",function.getCJCAmount());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("BussinessPartenName",function.getBussineePartnerName());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("InitaiatedBy",function.getInitaiatedBy());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("certifier",function.getcertifier());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("Approver",function.getApprover());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("DateOfCompletion",function.getDateOfCompletion());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,CJC_FormActivity.class);
                Bundle param=new Bundle();
                param.putString("PaymentDueDate",function.getPaymentDueDate());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return CJCArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtCJCNo;
        TextView txtOrderNo;
        TextView txtCJCDate;
        TextView txtOrderValue;
        TextView txtCJCAmount;
        TextView txtBussinessPartenName;
        TextView txtInitaiatedBy;
        TextView txtcertifier;
        TextView txtApprover;
        TextView txtDateOfCompletion;
        TextView txtPaymentDueDate;

        CardView cv_function_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCJCNo = itemView.findViewById(R.id.txtCJCNo);
            txtOrderNo= itemView.findViewById(R.id.txtOrderNo);
            txtCJCDate = itemView.findViewById(R.id.txtCJCDate);
            txtOrderValue = itemView.findViewById(R.id.txtOrderValue);
            txtCJCAmount = itemView.findViewById(R.id.txtCJCAmount);
            txtBussinessPartenName = itemView.findViewById(R.id.txtBussinessPartenName);
            txtInitaiatedBy = itemView.findViewById(R.id.txtInitaiatedBy);
            txtcertifier = itemView.findViewById(R.id.txtcertifier);
            txtApprover = itemView.findViewById(R.id.txtApprover);
            txtDateOfCompletion = itemView.findViewById(R.id.txtDateOfCompletion);
            txtPaymentDueDate = itemView.findViewById(R.id.txtPaymentDueDate);

            cv_function_card = itemView.findViewById(R.id.cv_function_card);


        }
    }

}
