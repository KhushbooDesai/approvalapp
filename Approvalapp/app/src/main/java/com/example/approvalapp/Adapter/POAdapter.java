package com.example.approvalapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.approvalapp.Model.POParam;
import com.example.approvalapp.R;

import java.util.ArrayList;
import java.util.List;


public class POAdapter extends RecyclerView.Adapter<POAdapter.RecyclerHolder> {



    Context context;
    ArrayList<POParam> functionArrayList;


    public POAdapter(Context context, ArrayList<POParam> functionsArrayList)
    {
        this.context = context;
        this.functionArrayList = functionsArrayList;
    }

    @NonNull
    @Override

    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row, parent, false);
        return new RecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {

        final POParam poParam=functionArrayList.get(holder.getAdapterPosition());
        holder.tv1.setText(poParam.getPurchaseorder());
        holder.tv2.setText(poParam.getAmendment());
        holder.tv3.setText(poParam.getBuyer());
        holder.tv4.setText(poParam.getBPCode());
        holder.tv5.setText(poParam.getDescription());
        holder.tv6.setText(poParam.getStatus());
        holder.tv7.setText(poParam.getICName());
        holder.tv8.setText(poParam.getPsname());


    }

    @Override
    public int getItemCount() {
        return functionArrayList.size();
    }

    public class RecyclerHolder extends RecyclerView.ViewHolder{

        TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8;
        public RecyclerHolder(@NonNull View itemView) {


            super(itemView);
            tv1 = itemView.findViewById(R.id.PONum);
            tv2 = itemView.findViewById(R.id.Amendment);
            tv3 = itemView.findViewById(R.id.Buyer);
            tv4 = itemView.findViewById(R.id.BPCode);
            tv5 = itemView.findViewById(R.id.Desc);
            tv6 = itemView.findViewById(R.id.Status);
            tv7 = itemView.findViewById(R.id.IC);
            tv8 = itemView.findViewById(R.id.PsName);

        }
    }

}
