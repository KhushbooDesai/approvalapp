package com.example.approvalapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.approvalapp.Model.PPO_Param;
import com.example.approvalapp.R;
import com.example.approvalapp.activity.MainActivity;

import java.util.ArrayList;

public class PPO_Adapter extends RecyclerView.Adapter<PPO_Adapter.MyViewHolder> {
    Context context;
    ArrayList<PPO_Param> ppoArrayList;

    public PPO_Adapter(Context context, ArrayList<PPO_Param> ppoArrayList) {
        this.context = context;
        this.ppoArrayList = ppoArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_ppo,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
        //return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final PPO_Param ppo = ppoArrayList.get(holder.getAdapterPosition());
        holder.txtPPONo.setText(ppo.getPPONo());
        holder.txtInitiatoName.setText(ppo.getInitiatorName());
        holder.txtOrderQty.setText(ppo.getOrderQty());
        holder.txtIC.setText(ppo.getIC());
        holder.txtStatus.setText(ppo.getStatusDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, MainActivity.class);
                Bundle param=new Bundle();
                param.putString("PPONo",ppo.getPPONo());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ppoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtPPONo,txtInitiatoName,txtOrderQty,txtIC,txtStatus;
        CardView cv_function_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtPPONo = itemView.findViewById(R.id.txtPPONo);
            txtInitiatoName = itemView.findViewById(R.id.txtInitiatoName);
            txtOrderQty = itemView.findViewById(R.id.txtOrderQty);
            txtIC = itemView.findViewById(R.id.txtIC);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            cv_function_card = itemView.findViewById(R.id.cv_function_card);
        }
    }
}
