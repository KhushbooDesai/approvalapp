package com.example.approvalapp.Interface;


import com.example.approvalapp.Model.APRList;
import com.example.approvalapp.Model.CJC;
import com.example.approvalapp.Model.IconList;
import com.example.approvalapp.Model.LoginResponse;
import com.example.approvalapp.Model.POList;
import com.example.approvalapp.Model.PPO;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api
{

   // @FormUrlEncoded
    //login is the  controller method
    @GET("Login")
    //PSNO is table field
    Call<LoginResponse> loginCheck(@Query("Psno") String psno, @Query("Password") String Password);

    @GET("ListOfApp")
    Call<IconList> listofapp();

    @GET("GetPPOData")
    Call<PPO> GetPPOData(@Query("Psno") String Psno);

   @GET("GetPOData")
   Call<POList> getallPO(@Query("Psno") String Psno);

    @GET("GetAPRData")
    Call<APRList> GetAPRData(@Query("Psno") String psno);

    //CJC
    @GET("GetCJCData")
    Call<CJC>GetCJCData(@Query("PSNO") String psno);

}
