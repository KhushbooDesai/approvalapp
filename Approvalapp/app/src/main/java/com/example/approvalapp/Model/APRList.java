package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class APRList {

    @SerializedName("Result")
    private String Result;

    @SerializedName("Code")
    private String Code;

    @SerializedName("Message")
    private String Message;

    @SerializedName("Data")
    private ArrayList<APRParam> aprParamArrayListArrayList;

    public String getResult() {
        return Result;
    }
    public void setResult(String result) {
        Result = result;
    }

    public String getCode() {
        return Code;
    }
    public void setCode(String code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }
    public void setMessage(String message) {
        Message = message;
    }




    public ArrayList<APRParam> getAPRArrayList()
    {
        return aprParamArrayListArrayList;
    }
    public void setAPRArrayList(ArrayList<APRParam> aprParamArrayListArrayList)
    {
        this.aprParamArrayListArrayList = aprParamArrayListArrayList;
    }
}
