package com.example.approvalapp.Model;
import com.google.gson.annotations.SerializedName;
public class APRParam {
    @SerializedName("RequestID")
    private String RequestID;

    @SerializedName("OrderNo")
    private String OrderNo;

    @SerializedName("Amount")
    private String Amount;

    @SerializedName("Remarks")
    private String Remarks;

    @SerializedName("CreatedBy")
    private String CreatedBy;

    @SerializedName("CreatedByName")
    private String CreatedByName;

    @SerializedName("BPID")
    private String BPID;

    @SerializedName("BPName")
    private String BPName;

    @SerializedName("IC")
    private String IC;

    @SerializedName("Status")
    private String Status;

    public String getRequestId() {
        return RequestID;
    }
    public void setRequestId(String requestId) {
        RequestID = requestId;
    }

    public String getOrderNo() {
        return OrderNo;
    }
    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getAmount() {
        return Amount;
    }
    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getRemarks() {
        return Remarks;
    }
    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }
    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedByName() {
        return CreatedByName;
    }
    public void setCreatedByName(String createdByName) {
        CreatedByName = createdByName;
    }

    public String getBPId() {
        return BPID;
    }
    public void setBPId(String bPId) {
        BPID = bPId;
    }

    public String getBPName() {
        return BPName;
    }
    public void setBPName(String bPName) {
        BPName = bPName;
    }

    public String getIC() {
        return IC;
    }
    public void setIC(String ic) {
        IC = ic;
    }

    public String getStatus() {
        return Status;
    }
    public void setStatus(String status) {
        Status = status;
    }
}
