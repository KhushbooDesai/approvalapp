package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CJC {
    @SerializedName("Code")
    private String Code;
    @SerializedName("Data")
    private ArrayList<CJCParam> CJCArrayList;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public ArrayList<CJCParam> getCJCArrayList()
    {
        return CJCArrayList;
    }

    public void setUserArrayList(ArrayList<CJCParam> functionArrayList)
    {
        this.CJCArrayList = functionArrayList;
    }
}
