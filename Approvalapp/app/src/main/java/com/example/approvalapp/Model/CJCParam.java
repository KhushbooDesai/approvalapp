package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

public class CJCParam {
    @SerializedName("CJCNo")
    private String CJCNo;

    @SerializedName("OrderNo")
    private String OrderNo;

    @SerializedName("CJCDate")
    private String CJCDate;

    @SerializedName("OrderValue")
    private String OrderValue;

    @SerializedName("Amount")
    private String Amount;

    @SerializedName("BussineePartnerName")
    private String BussineePartnerName;

    @SerializedName("CheckedByName")
    private String CheckedByName;

    @SerializedName("CertifierName")
    private String CertifierName;

    @SerializedName("ApproverName")
    private String ApproverName;

    @SerializedName("DateOfCompletion")
    private String DateOfCompletion;

    @SerializedName("PaymentDueDate")
    private String PaymentDueDate;

    @SerializedName("ColorCode")
    private String ColorCode;

    public String getCJCNo() {
        return CJCNo;
    }

    public void setCJCNo(String CJCNo) {
        this.CJCNo = CJCNo;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getCJCDate() {
        return CJCDate;
    }

    public void setCJCDate(String CJCDate) {
        this.CJCDate = CJCDate;
    }

    public String getOrderValue() {
        return OrderValue;
    }

    public void setOrderValue(String orderValue) {
        OrderValue = orderValue;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getBussineePartnerName() {
        return BussineePartnerName;
    }

    public void setBussineePartnerName(String bussineePartnerName) {
        BussineePartnerName = bussineePartnerName;
    }

    public String getCheckedByName() {
        return CheckedByName;
    }

    public void setCheckedByName(String checkedByName) {
        CheckedByName = checkedByName;
    }

    public String getCertifierName() {
        return CertifierName;
    }

    public void setCertifierName(String certifierName) {
        CertifierName = certifierName;
    }

    public String getApproverName() {
        return ApproverName;
    }

    public void setApproverName(String approverName) {
        ApproverName = approverName;
    }

    public String getDateOfCompletion() {
        return DateOfCompletion;
    }

    public void setDateOfCompletion(String dateOfCompletion) {
        DateOfCompletion = dateOfCompletion;
    }

    public String getPaymentDueDate() {
        return PaymentDueDate;
    }

    public void setPaymentDueDate(String paymentDueDate) {
        PaymentDueDate = paymentDueDate;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }
}
