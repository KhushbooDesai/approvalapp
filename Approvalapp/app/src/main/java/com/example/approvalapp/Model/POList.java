package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
public class POList {
    @SerializedName("Code")
    private String Code;
    @SerializedName("Data")
    private ArrayList<POParam> functionArrayList;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public ArrayList<POParam> getFunctionArrayList()
    {
        return functionArrayList;
    }

    public void setUserArrayList(ArrayList<POParam> functionArrayList)
    {
        this.functionArrayList = functionArrayList;
    }



}
