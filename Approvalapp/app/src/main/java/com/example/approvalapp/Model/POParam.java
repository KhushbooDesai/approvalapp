package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;
public class POParam {
    @SerializedName("Purchaseorder")
    private String Purchaseorder;

    @SerializedName("Description")
    private String Description;

    @SerializedName("Seq")
    private String Seq;

    @SerializedName("Amendment")
    private String Amendment;

    @SerializedName("BPCode")
    private String BPCode;

    @SerializedName("Status")
    private String Status;

    @SerializedName("Buyer")
    private String Buyer;

    @SerializedName("Psname")
    private String Psname;

    @SerializedName("ICName")
    private String ICName;

    public String getSeq() {
        return Seq;
    }

    public void setSeq(String seq) {
        Seq = seq;
    }

    public String getAmendment() {
        return Amendment;
    }

    public void setAmendment(String amendment) {
        Amendment = amendment;
    }

    public String getBPCode() {
        return BPCode;
    }

    public void setBPCode(String BPCode) {
        this.BPCode = BPCode;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getBuyer() {
        return Buyer;
    }

    public void setBuyer(String buyer) {
        Buyer = buyer;
    }

    public String getPsname() {
        return Psname;
    }

    public void setPsname(String psname) {
        Psname = psname;
    }

    public String getICName() {
        return ICName;
    }

    public void setICName(String ICName) {
        this.ICName = ICName;
    }

    public String getPurchaseorder() {
        return Purchaseorder;
    }

    public void setPurchaseorder(String purchaseorder) {
        Purchaseorder = purchaseorder;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
