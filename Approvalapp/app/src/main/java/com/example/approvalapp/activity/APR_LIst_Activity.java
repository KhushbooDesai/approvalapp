package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.APRList;
import com.example.approvalapp.R;
import com.example.approvalapp.Adapter.APR_APRList_Adapter;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

public class APR_LIst_Activity extends AppCompatActivity {


    ImageView logoutimg;
    TextView userPsno;
    String usernm, Psno;

    Api api;
    RecyclerView recycler_view;
    SharedPref sharedPref;
    LinearLayoutManager linearLayoutManager;
    private APR_APRList_Adapter aprListAdapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apr_list);
        sharedPref = new SharedPref(APR_LIst_Activity.this);
        Log.e("Flag ", "Came onCreate");
        userPsno = findViewById(R.id.txtName);
        usernm = sharedPref.getString("userPsno");
        userPsno.setText("  WELCOME " + usernm);

        initView();
        initViewAction();

        logoutimg.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                sharedPref.putBoolean("IsFirstTimeLaunch", false);
                sharedPref.putString("userName", "");
                sharedPref.putString("PSNO", "");
                sharedPref.putString("date", "");
                sharedPref.putString("userPSNO", "");
                sharedPref.putString("userPassword", "");
                sharedPref.putString("Role", "");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        });

    }

    public void initView() {
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setNestedScrollingEnabled(true);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(linearLayoutManager);
        logoutimg = findViewById(R.id.img_logout);
    }

    private void initViewAction() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading..");
        progressDialog.show();

        Psno = sharedPref.getString("userPsno");
        api = RetrofitAPIClient.getClient().create(Api.class);

        Call<APRList> aprListData = api.GetAPRData(Psno);//115577


        aprListData.enqueue(new Callback<APRList>() {
            @Override
            public void onResponse(Call<APRList> call, Response<APRList> response) {
                if (response.isSuccessful()) {

                    if (response.isSuccessful()) {
                        Log.e("response from service: ", response.body().getCode());
                        if (response.body().getCode().equals("200")) {
                            if (response.body().getAPRArrayList().size() > 0 &&
                                    response.body().getAPRArrayList() != null) {
                                setAdapter(response.body());
                                //Log.e("TAG", "onResponse: "+ response.body().getAPRArrayList().size());
                                progressDialogDismiss();
                            } else {
                                Toast.makeText(APR_LIst_Activity.this, "No Data Found.", Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(getApplicationContext(), IconListActivity.class);
                                startActivity(intent);
                                progressDialogDismiss();
                            }
                        } else {
                            Toast.makeText(APR_LIst_Activity.this, "Something went wrong", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getApplicationContext(), IconListActivity.class);
                            startActivity(intent);
                            progressDialogDismiss();
                        }
                    }
                }
                else
                {
                    Toast.makeText(APR_LIst_Activity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                    progressDialogDismiss();
                }
            }

            @Override
            public void onFailure(Call<APRList> call, Throwable t) {
                Log.e("TAG", "Error While Receiving Response");
            }
        });
    }

    private void setAdapter(APRList aprList) {
        aprListAdapter = new APR_APRList_Adapter(this, aprList.getAPRArrayList());
        recycler_view.setAdapter(aprListAdapter);
    }

    private void progressDialogDismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
