package com.example.approvalapp.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.approvalapp.Adapter.CJCAdapter;
import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.CJC;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CJC_ListActivity extends AppCompatActivity {

    ImageView logoutimg;
    TextView userPsno;
    String usernm;

    //TextView txtLastvisted, txtFrequent1,txtFrequent2, txtFrequent3 ;
    RecyclerView rv_CJC;
    LinearLayoutManager LinearLayoutManager;
//    CardView cv_frequent2,cv_frequent3;

    private CJCAdapter CJCAdapter;

    SharedPref sharedPref;
    SimpleDateFormat df;
    Date currentDate;
    String todayDate;
// public final String txt1="";

    ProgressDialog dialog;
    Api api;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_j_c);
       sharedPref = new SharedPref(CJC_ListActivity.this);

        initView();
        initViewAction();
//        initViewListener();
        userPsno=findViewById(R.id.txtName);
        usernm=sharedPref.getString("userPsno");
        userPsno.setText("  WELCOME "+usernm);


        logoutimg.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

             startActivity(intent);
            }
        });

    }
        //TextView txtLastvisted, txtFrequent1,txtFrequent2, txtFrequent3 ;


        private void initView()
        {
            rv_CJC = findViewById(R.id.rv_CJC);
            rv_CJC.setNestedScrollingEnabled(true);
            logoutimg = findViewById(R.id.img_logout);

            LinearLayoutManager = new LinearLayoutManager(CJC_ListActivity.this,LinearLayoutManager.VERTICAL,false);
            rv_CJC.setLayoutManager(LinearLayoutManager);
        }
        private void initViewAction()
        {
            dialog = new ProgressDialog(CJC_ListActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage("Loading..");
            dialog.show();

            String Psno;
            Psno = sharedPref.getString("userPsno"); //90348357
            api = RetrofitAPIClient.getClient().create(Api.class);

            try
            {
                //for get function
                Call<CJC> callFucntion = api.GetCJCData(Psno);
                callFucntion.enqueue(new Callback<CJC>() {
                    @Override
                    public void onResponse(Call<CJC> call, Response<CJC> response) {
                        if (response.isSuccessful()){
                            if(response.body().getCode().equals("200")){
                                if(response.body().getCJCArrayList().size()>0 &&
                                        response.body().getCJCArrayList() != null){
                                    setAdapter(response.body());
                                    Log.e("PPO","LIST :"+response.body().getCJCArrayList().size());
                                    forProgressDialog();
                                }
                                else{
                                    Toast.makeText(CJC_ListActivity.this, "No Data Found.", Toast.LENGTH_LONG).show();

                                    Intent intent=new Intent(getApplicationContext(),IconListActivity.class);
                                    startActivity(intent);
                                    forProgressDialog();
                                }
                            }
                            else {
                                Toast.makeText(CJC_ListActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();

                                Intent intent=new Intent(getApplicationContext(),IconListActivity.class);
                                startActivity(intent);
                                forProgressDialog();
                            }
                        }
                        else
                        {
                            Toast.makeText(CJC_ListActivity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                            forProgressDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<CJC> call, Throwable t) {
                        Toast.makeText(CJC_ListActivity.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                        Log.e("Error",t.getMessage());
                        forProgressDialog();
                    }
                });
            }
            catch (Exception e)
            {
                Log.e("Exception",e.getMessage());
                forProgressDialog();
            }
        }

        public void forProgressDialog()
        {
            if(dialog.isShowing())
            {
                dialog.dismiss();
            }
        }


        private void setAdapter(CJC CJCList)
        {
            CJCAdapter =new CJCAdapter(getApplicationContext(),CJCList.getCJCArrayList());
            rv_CJC.setAdapter(CJCAdapter);
            forProgressDialog();
//        progressDialog.dismiss();
        }



    }


