package com.example.approvalapp.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;

import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.IconList;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import retrofit2.Call;

public class CommonFragment extends Fragment {

    TextView txtName;
    View view;
    SharedPref sharedPref;
    String userPsno;
    Api api;

    public CommonFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_common, container, false);
        sharedPref = new SharedPref(getActivity());
        initView();
        initViewAction();
        initViewListener();
        return view;
    }
    private void initView() {


    }
    private void initViewAction() {

        txtName.setText(sharedPref.getString("userName"));
    }
    private void initViewListener()
    {

    }
    @Override
    public void onResume() {
        super.onResume();
        userPsno = sharedPref.getString("userPsno");
        api = RetrofitAPIClient.getClient().create(Api.class);
       }
    }
