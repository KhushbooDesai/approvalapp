package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.approvalapp.Adapter.ListOfAppAdapter;
import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.IconList;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import okhttp3.internal.Version;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IconListActivity extends AppCompatActivity implements View.OnClickListener {
   CardView ppocardview,pocardview,cjccardview,apscardview;

    ImageView logoutimg;
    TextView userPsno;
    String usernm;

    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icon_list);

        sharedPref = new SharedPref(IconListActivity.this);

        initView();
        initViewAction();
        initViewListener();
        //show name
        userPsno=findViewById(R.id.txtName);
        usernm=sharedPref.getString("userPsno");
        userPsno.setText("  WELCOME "+usernm);


        logoutimg.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        });
    }

    private void initView()
    {
       ppocardview=findViewById(R.id.ppo_card);
       pocardview=findViewById(R.id.po_card);
       apscardview=findViewById(R.id.aps_card);
       cjccardview=findViewById(R.id.cjc_card);
       ppocardview.setOnClickListener(this);
        pocardview.setOnClickListener(this);
        cjccardview.setOnClickListener(this);
        apscardview.setOnClickListener(this);
        logoutimg=findViewById(R.id.img_logout);
    }
    private void initViewAction() {
    }

    private void  initViewListener()
    {

    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.ppo_card:
                i = new Intent(getApplicationContext(),Ppo_ListActivity.class);
                startActivity(i);
                break;
            case R.id.po_card:
                i = new Intent(getApplicationContext(),PO_Activity.class);
                startActivity(i);
                break;
            case R.id.aps_card:
                i = new Intent(getApplicationContext(),APR_LIst_Activity.class);
                startActivity(i);
                break;
            case R.id.cjc_card:
                i = new Intent(getApplicationContext(),CJC_ListActivity.class);
                startActivity(i);
                break;
            default:break;

        }

    }
}
