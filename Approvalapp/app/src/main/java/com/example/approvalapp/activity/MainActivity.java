package com.example.approvalapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.approvalapp.Model.IconList;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.SharedPref;

public class MainActivity extends AppCompatActivity {
    MainActivity activity;
    SharedPref sharedPref;
    String Password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = MainActivity.this;
        sharedPref = new SharedPref(activity);

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                //First Time User
                if(!sharedPref.getBoolean("IsFirstTimeLaunch"))
                {
                    Intent intent = new Intent(activity,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Intent intent = new Intent(activity,IconListActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },2000);

    }
}
