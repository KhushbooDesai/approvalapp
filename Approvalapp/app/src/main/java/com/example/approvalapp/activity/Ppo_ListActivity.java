package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.approvalapp.Adapter.PPO_Adapter;
import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.PPO;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ppo_ListActivity extends AppCompatActivity {
    RecyclerView rv_PPO;
    ImageView logoutimg;
    TextView userPsno;
    String usernm;
    LinearLayoutManager linearLayoutManager;

    private PPO_Adapter ppoAdapter;
    Api api;
    ProgressDialog dialog;
    SharedPref sharedPref;
    String Psno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppo_list);
        sharedPref = new SharedPref(Ppo_ListActivity.this);

        Log.e("TAG", "onCreate: PPO_LIST" );
        initView();
        initViewAction();

        //show name
        userPsno=findViewById(R.id.txtName);
        usernm=sharedPref.getString("userPsno");
        userPsno.setText("  WELCOME "+usernm);

        logoutimg.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        });

    }

    private void initView() {
        rv_PPO = findViewById(R.id.rv_PPO);
        logoutimg = findViewById(R.id.img_logout);
        linearLayoutManager = new LinearLayoutManager(Ppo_ListActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_PPO.setLayoutManager(linearLayoutManager);
    }

    private void initViewAction()
    {
        dialog = new ProgressDialog(Ppo_ListActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading..");
        dialog.show();

        Psno = sharedPref.getString("userPsno");
        api = RetrofitAPIClient.getClient().create(Api.class);

        try {
            //for ppo list
            Call<PPO> ppoCall = api.GetPPOData(Psno);
            ppoCall.enqueue(new Callback<PPO>() {
                @Override
                public void onResponse(Call<PPO> call, Response<PPO> response) {
                    Log.e("TAG", "onResponse: "+response.body() );
                    if (response.isSuccessful()){
                        Log.e("Code", "onResponse: "+response.body().getCode() );
                        if(response.body().getCode().equals("200")){
                            if(response.body().getPpoArrayList().size()>0 &&
                                    response.body().getPpoArrayList() != null){
                                setAdapter(response.body());
                                Log.e("PPO","LIST :"+response.body().getPpoArrayList().size());
                                forProgressDialog();
                            }
                        else{
                                Toast.makeText(Ppo_ListActivity.this, "No Data Found.", Toast.LENGTH_LONG).show();

                                Intent intent=new Intent(getApplicationContext(),IconListActivity.class);
                                startActivity(intent);
                                forProgressDialog();
                            }
                        }
                        else {
                            Toast.makeText(Ppo_ListActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();

                            Intent intent=new Intent(getApplicationContext(),IconListActivity.class);
                            startActivity(intent);
                            forProgressDialog();
                        }
                    }
                    else
                    {
                        Toast.makeText(Ppo_ListActivity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        forProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<PPO> call, Throwable t) {
                    Toast.makeText(Ppo_ListActivity.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                    Log.e("Error",t.getMessage());
                    forProgressDialog();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setAdapter(PPO ppoList)
    {
        ppoAdapter =new PPO_Adapter(getApplicationContext(),ppoList.getPpoArrayList());
        rv_PPO.setAdapter(ppoAdapter);
        //forProgressDialog();
    }

    public void forProgressDialog()
    {
        if(dialog.isShowing())
        {
            dialog.dismiss();
        }
    }
}
